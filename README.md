
# Установка
(предполагается, что ваша основная система Ubuntu)
## Клонировать репозиторий
* <a href="https://computingforgeeks.com/how-to-install-php-7-3-on-ubuntu-18-04-ubuntu-16-04-debian/">**установить php 7.3**</a>  
* **sudo apt install git** - установить git  
* **git clone git@gitlab.com:Rafail/zioneck.git** - клонировать проект  

## Настроить окружение
* Создать файл  **.env** используя как пример **.env.example**  
* Открыть файл **.env** и настроить порты и xdebug при необходимости

## Установить composer
* <a href="https://getcomposer.org/download/">**скачать composer**</a>
* <a href="https://getcomposer.org/doc/00-intro.md#globally">**установить composer глобально**</a>
* **composer install** - установить зависимости приложения

## Установить docker
* <a href="https://docs.docker.com/install/linux/docker-ce/ubuntu/">**docker**</a>  
* <a href="https://docs.docker.com/install/linux/linux-postinstall/">**linux-postinstall**</a> (обязательные шаги после установки, необходимо перелогиниться)  
* <a href="https://docs.docker.com/compose/install/">**docker-compose**</a>

## Запустить Docker
* **docker-compose up -d --build** первый запуск докера (создать сборку и запустить)  
Второй и последующие запуски:  
* **docker-compose up -d** запустить сборку  
* **docker-compose down** остановить  
**При необходимости может понадобиться удалить различные компоненты, которые создает Docker**. <a href="https://www.digitalocean.com/community/tutorials/how-to-remove-docker-images-containers-and-volumes">**Подробнее**</a>

## Загрузить данные в БД
* **docker-compose exec app bash** открыть консоль внутри app(php/nginx) контейнера как **root**  
* **./bin/console doctrine:migrations:migrate** загрузить миграции БД  
* **./bin/console doctrine:fixtures:load** загрузить тестовые данные в БД (используется <a href="https://github.com/fzaninotto/Faker">**fzaninotto/faker**</a>)  

## Настройки докера по умолчанию для внешней хост машины (внутри контейнеров порты стандартные)
* Порт web-server **8100** (http://localhost:8100)  
* Порт mysql **13310**  
    * Имя БД **zioneck**  
    * Имя пользователя БД **zioneck**  
    * Пароль БД **zioneck**  

## API
* http://localhost:8100/api  
