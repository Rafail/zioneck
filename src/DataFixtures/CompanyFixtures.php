<?php


namespace App\DataFixtures;


use App\Entity\Company;
use Doctrine\Common\Persistence\ObjectManager;

class CompanyFixtures extends BaseFixture
{
    protected function loadData(ObjectManager $manager)
    {
        $this->createMany(10, 'companies', function ($i) use ($manager) {

            $company = new Company();
            $company
                ->setName($this->faker->company)
                ->setAddress($this->faker->address);

            return $company;
        });

        $manager->flush();
    }
}